#!/usr/bin/env python
# Copyright (c) 2019,2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

# cat access.log | python ./apache2able.py '/css/' 'get' > ~/get_css.csv

from __future__ import with_statement

from os import path,sep
from datetime import datetime as dt
import re
import shlex
import subprocess
from sys import argv,stdin

from string import ascii_letters,digits,maketrans,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))
DOUBLEQUOTES_INSTEAD = maketrans('[]', '""')

""" Common log format parsing on the fly?
Use shlex.split(logline) or unicodey shlex.split(logline.encode('utf8')) or your friendly csv module
for row in csv.reader(lines, delimiter=" "):
"""

def process_lines(lines_unsplit,request_pattern=None,request_method=None,status_code=200):
	"""
	127.0.0.1 - someuser [21/Sep/2016:13:55:36 -0700] "GET /valid-xhtml10-blue.png HTTP/1.0" 200 447
	"""
        #print request_pattern,request_method
        if request_method is None:
                pass
        elif request_method in ['get','GET']:
                request_method = 'get'
        elif request_method in ['post','post']:
                request_method = 'post'
        else:
                pass
        #print request_pattern,request_method
        
	tmatching_dict = {}
	for idx,line in enumerate(lines_unsplit):

		line_array = shlex.split(line.translate(DOUBLEQUOTES_INSTEAD))
		ipaddr = line_array[0]
		ident = line_array[1]
		user = line_array[2]
		request_dt = line_array[3]
		request = line_array[4]
		status = line_array[5]
		try:
			request_bytes = line_array[6]
			request_int = int(request_bytes)
		except:
			err_str = "Error processing access log output line %s" % idx
			err_str = "%s having request and status of %s" % (err_str,line_array[4:5])
			raise RuntimeError(err_str)

		request_array = shlex.split(request)
                try:
                        rmethod = request_array[0].strip("""'\"""").lower()
                except:
			err_str = "Error processing access log output line %s" % idx
			err_str = "%s having request of %s" % (err_str,request)
			raise RuntimeError(err_str)
                        
                if request_method is not None:
                        # We are being asked to pay attention only to supplied request_method
                        if rmethod != request_method:
                                continue

		try:
			status_int = int(status)
		except:
			err_str = "Error processing access log output line %s" % idx
			err_str = "%s having status=%s of %s" % (err_str,status,line_array[4])
			raise RuntimeError(err_str)

		if status_code is None:
			pass
		elif status_code > 0:
			if status_code == status_int:
				pass
			else:
				continue
		else:
			pass

                request_uri = request_array[1]
                request_version = request_array[2]
                if request_pattern is not None:
                        #print request_pattern,request_uri
                        if request_pattern in request_uri:
                                pass
                        else:
                                continue
                
		combined_key = "{0},{1},{2}".format(request_int,request_method,request_uri)
                #print combined_key

		if combined_key in tmatching_dict:
			list_count_and_dates = tmatching_dict[combined_key]
			list_count_and_dates[0] = 1+list_count_and_dates[0]
			list_count_and_dates[2] = request_dt
			tmatching_dict[combined_key] = list_count_and_dates
		else:
			list_count_and_dates = [1,request_dt,request_dt]
			tmatching_dict[combined_key] = list_count_and_dates

	return tmatching_dict


if __name__ == "__main__":

	exit_rc = 0

	DEBUG = False

	pattern_given = 'all'
        request_method = 'None'
	if len(argv) > 1:
		pattern_given = argv[1]
                if len(argv) > 2:
                        request_method = argv[2]

	if pattern_given == 'all':
		pattern_given = None
	elif len(pattern_given) < 2:
		pattern_given = None
	else:
		pass

	if request_method == 'all':
		request_method = None
	elif len(request_method) < 3:
		request_method = None
	else:
		pass

	try:
		matching_dict = process_lines(stdin.readlines(),pattern_given,request_method)
	except:
		exit_rc = 141
		raise

	if 0 == exit_rc and len(matching_dict) < 1:
		exit_rc = 150


        # chr(44) is comma
        for combined_key,list_count_and_dates in matching_dict.items():
                #print list_count_and_dates
                count = list_count_and_dates[0]
                combined_array = combined_key.split(chr(44))
                request_uri = combined_array[2]
                count_method_dated = "{0},{1},{2},{3}".format(count,combined_array[1],
                                                              list_count_and_dates[1],
                                                              list_count_and_dates[2])
                print "{0},{1},{2}".format(combined_array[0],count_method_dated,request_uri)

	exit(exit_rc)



