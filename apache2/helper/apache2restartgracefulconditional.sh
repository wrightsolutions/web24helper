#!/bin/sh
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

DT_=$(/bin/date +%Y%m%dT%H%M)
printf '%s ... configtest then graceful restart ... ' "${DT_}"
/usr/sbin/apache2ctl configtest
[ $? -eq 0 ] && /usr/sbin/apache2ctl -k graceful
RC_=$?
DT_=$(/bin/date +%Y%m%dT%H%M)
printf 'completed at %s\n' "${DT_}"
exit ${RC_}
