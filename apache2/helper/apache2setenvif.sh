#!/bin/dash
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

D_=/etc/apache2/conf.d
[ ! -d ${D_} ] && exit 101
cat > ${D_}/setenvif-local <<EOF
SetEnvIf Request_URI "admin/scripts/.*\.php$" envflag
SetEnvIf Request_URI "config\.inc\.php\?p=phpinfo" envflag
SetEnvIf Request_URI "[aA][dD][mM].*/scripts/setup\.php$" envflag
SetEnvIf Request_URI "[pP][mM][aA].*/scripts/setup\.php$" envflag
SetEnvIf Request_URI "favicon\.ico$" envflag
SetEnvIf Request_URI ".*/garland.*/b.*\.png$" envflag
SetEnvIf Request_URI ".*\.css\?5\.css$" envflag
EOF
