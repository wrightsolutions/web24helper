#!/bin/sh
#   Copyright 2014 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

# You have a Python based cms in /var/www/mycms: apache2graceful_cleared.sh 'www/mycms'

DT_=$(/bin/date +%Y%m%dT%H%M)
printf '%s ... configtest then graceful restart ... ' "${DT_}"
FINDBIN_=/usr/bin/find
#VARAPPEND_='www/dynamic'
VARAPPEND_='www'
/usr/sbin/apache2ctl configtest
if [ $? -eq 0 ]; then
    if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
	    VARAPPEND_=$1
	fi
    fi
    printf 'Removing pyc files from /var/%s\n' "${VARAPPEND_}"
    ${FINDBIN_} /var/${VARAPPEND_}/ -type f -name '*.pyc' -exec rm -f {} \;
    /usr/sbin/apache2ctl -k graceful
    RC_=$?
fi
DT_=$(/bin/date +%Y%m%dT%H%M)
printf 'completed at %s\n' "${DT_}"
exit ${RC_}
