#!/bin/sh
# Copyright (c) 2019,2013 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

# Benchmark support script. Django related.
# Dump data, duplicate it many times in stream, then has that stream.
# Might take between 30 seconds and 60 on a cheap / slow cloud server

[ $# -lt 1 ] && exit 101
DIR_PROJECT_=$1
DTISO_=$(/bin/date +%Y%m%dT%H%M%S)
SLEEP_DURATION_=60
printf "$DTISO_\n"
printf "$SED16_\n"
if [ -d ${DIR_PROJECT_} ]; then
    cd ${DIR_PROJECT_}
    ./manage.py dumpdata | /usr/bin/head -c 1000000 | \
	/bin/sed -n 'p;p;p;p;p;p;p;p;p;p' | \
	/bin/sed -n 'p;p;p;p;p;p;p;p;p;p' | \
	/bin/sed -n 'p;p;p;p;p;p;p;p;p;p' | \
	/bin/sed -n 'p;p;p;p;p;p;p;p;p;p' | \
	/usr/bin/sha256sum
    printf "%s\n" $(/bin/date +%Y%m%dT%H%M%S)
else
    printf "Sleeping for $SLEEP_DURATION_ seconds\n"
    sleep ${SLEEP_DURATION_}
fi
